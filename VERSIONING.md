## Branching System

  We will have 2 main branches:
  - **Master**, with next tag ( staging )
  - **Prod**, with lastest tag ( production ) 

## Standards
  We use conventional commits and semantic releases.
[ conventional commit rules used ](https://www.conventionalcommits.org/en/v1.0.0/)
## Publishing
  We will publish based on semnatic release:
  - fix - patch
  - BREAKING CHANGE - major release
  - any other type is a minor release

  We will use independent release
  
  Publishes from Master will be using **next** tag and will be suffixed with alpha

  Publishes from Prod will be using **latest** tag

