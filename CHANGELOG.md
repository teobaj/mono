# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.1-alpha.1](https://git.everymatrix.com/group-rnd/widgets/compare/v1.1.1-alpha.0...v1.1.1-alpha.1) (2021-03-29)


### Bug Fixes

* package.json for betting history ([d879cc3](https://git.everymatrix.com/group-rnd/widgets/commits/d879cc34e8b924f8df1b146928ff7abac39b1e5c))





## [1.1.1-alpha.0](https://git.everymatrix.com/group-rnd/widgets/compare/v1.1.0...v1.1.1-alpha.0) (2021-03-29)


### Reverts

* Revert "stop tracking yarn.lock as it changes way to often" ([6616fd1](https://git.everymatrix.com/group-rnd/widgets/commits/6616fd166bb2f2e21ad97e45351fcae42f2e0f6a))





# [1.1.0](https://git.everymatrix.com/group-rnd/widgets/compare/v1.1.0-alpha.0...v1.1.0) (2021-03-25)

**Note:** Version bump only for package @rnd/widgets





# 1.1.0-alpha.0 (2021-03-25)


### Features

* add test.txt to retail actvity history widget ([49edb7f](https://git.everymatrix.com/group-rnd/widgets/commits/49edb7f90046efc848fedbbcf983c208dcc95f08))





## 1.0.1-alpha.0 (2021-03-25)

**Note:** Version bump only for package @rnd/widgets
