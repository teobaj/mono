import React, { useState, useEffect } from 'react';
import { addons, types } from '@storybook/addons';
import { FORCE_RE_RENDER } from '@storybook/core-events';

import { AddonPanel } from '@storybook/components';

const ADDON_ID = 'login';
const PANEL_ID = `${ADDON_ID}/panel`;

// give a unique name for the panel
// ¿¿¿¿ 
const MyPanel = () => {
  const [session, setSession] = useState('')
  const setGlobalSession = () => {
    // setGlobals({['sessionId']: session})
    localStorage.setItem('sessionId', session)
    addons.getChannel().emit(FORCE_RE_RENDER);
  }

  return (
    <div>
      <label>SESSION</label>
      <input style={{margin: "10px"}} type="text" onChange={(e) => setSession(e.target.value)} value={session}></input>
      <button onClick={()=>setGlobalSession()}>Add session</button>
    </div>
  )
}



addons.register(ADDON_ID, (api) => {
  addons.add(PANEL_ID, {
    type: types.PANEL,
    title: 'Login',
    render: ({ active, key }) => (
      <AddonPanel active={active} key={key}>
        <MyPanel />
      </AddonPanel>
    ),
  });
});