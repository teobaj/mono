// UPP
// import '../packages/player-accounts-page/dist/player-accounts-page.js';
// import '../packages/player-accounts-table/dist/player-accounts-table.js';
// import '../packages/player-info-card/dist/player-info-card.js';
// import '../packages/player-risk-info-card/dist/player-risk-info-card.js';
// // import '../packages/player-audit-page/dist/player-audit-page.js';
// import '../packages/player-audit-table/dist/player-audit-table.js';
// import '../packages/player-balance-info-card/dist/player-balance-info-card.js';
// import '../packages/player-contact-details-card/dist/player-contact-details-card.js';
// import '../packages/player-bonus-changelog-table/dist/player-bonus-changelog-table.js';
// import '../packages/player-bonus-wallets-table/dist/player-bonus-wallets-table.js';

// Retail
// import '../packages/retail-markets/dist/retail-markets.js';

import './global.css';


window.addEventListener('message', (e) => {
  if(e.data?.type === 'GetAuthHeaders') {
    let data = localStorage.getItem('sessionId');
    window.postMessage({
    type: 'SetAuthHeaders', payload: {
      "X-SessionId": data,
      "X-Session-Type": "admin",
      "Content-Type": "application/json",
      "accept": "application/json",
    }
  })}
});

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}

// Global Session
export const globalTypes = {
  domain: {
    name: 'Domain',
    description: 'Widgets global domain',
    defaultValue: 'https://demo-api.stage.norway.everymatrix.com/v1/player/',
    toolbar: {
      icon: 'globe',
      items: [
        { value: 'https://demo-api.stage.norway.everymatrix.com/v1/player/', title: 'Demo' },
        { value: 'https://winmasters-api.stage.norway.everymatrix.com/v1/player/', title: 'Winmasters' },
        { value: 'https://jetbull-api.stage.norway.everymatrix.com/v1/player/', title: 'JetBull' },
      ],
      showName: true,
    }
  },
  sessionId: {
    name: 'sessionID',
    description: 'Widgets global domain',
    defaultValue: 'session',
    toolbar: {
      icon: 'globe',
      items: [
        { value: '', title: 'Demo' },
      ],
      showName: true,
    }
  },

}
