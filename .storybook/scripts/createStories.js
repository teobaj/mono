
const path = require("path");
const fs = require("fs");

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}
const componentName = (name) => {
  let arr = name.split('-');
  arr = arr.map(capitalizeFirstLetter);
  let newName = arr.join('');
  return newName
}

const varName = (name) => {
  let arr = name.split('-');
  arr = arr.map(capitalizeFirstLetter);
  arr.shift()
  let newName = arr.join('');
  return newName
}

const titleName = (name) => {
  let arr = name.split('-');
  arr.shift()
  arr = arr.map(capitalizeFirstLetter);
  let newName = arr.join(' ');
  return newName
}


let widgetsName = fs.readdirSync('packages/', { withFileTypes: true }).map(file => file.name).filter(name => name.includes('-') && !name.includes('retail') && name.includes('player') && !name.includes('helpers') && !name.includes('markets') && !name.includes('keyboard'));
//   let paths = widgetsName.map((name) => {
//     if(fs.existsSync(`packages/${name}/src/index.ts`)){
//       fs.appendFile('index.js', `import './packages/${name}/src/index.ts';`+'\n', 'utf8' , (err)=>console.log(err));
//     }
//   })
widgetsName.forEach((name) => {
  if (!fs.existsSync(`packages/${name}/stories/${componentName(name)}.stories.js`)) {
    if(!fs.existsSync(`packages/${name}/stories/`)) fs.mkdirSync(`packages/${name}/stories`);
    fs.appendFileSync(`packages/${name}/stories/${componentName(name)}.stories.js`, `import '../dist/${name}'` + '\n' + `export default { title: 'RETAIL Widget/${titleName(name)}',};` + '\n' + `let Template = ({playerId}, { globals: { domain } }) => { return \`<${name} src=\${domain}\${playerId}/audit></${name}>\`;};` + '\n' + `export const ${varName(name)} = Template.bind({});` + '\n' + `${varName(name)}.args = { playerId: '3592761' };` + '\n'
    ,'utf8', err => console.log(err))
    console.log('name', `packages/${name}/stories/${componentName(name)}.stories.js`)
  }
})
