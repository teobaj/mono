# {{namespace}}/{{name}}
---------

## First step
---------
```
  lerna bootstrap
```

## Run Widget
---------
- starts server at port 5050
- has livereload

```
  yarn run dev
```

## Build Widget
---------
```
  yarn run build
```

## Publishing widget
---------
- Note add your user to the npm
```
  lerna publish
```
