import { LitElement, html, css } from 'lit-element';

export default class {{ properCase name }} extends LitElement {
  render() {
    return html`
      <div class="webcomponent">Simple LitElement component!</div>
    `;
  }

  static get styles(){
    return css`
      *,
      *::before,
      *::after {
        margin: 0;
        padding: 0;
        list-style: none;
        outline: none;
        text-decoration: none;
        box-sizing: border-box;
        font-family: "Helvetica Neue", "Helvetica", sans-serif;
      }
      .webcomponent {
        background:#000;
        color: #fff;
      }
  `;
  }
}



!customElements.get('{{ dashCase name }}') && customElements.define('{{ dashCase name }}', {{ properCase name }});
