# Introduction
Access to the hardware on the Cashier Terminal and the SSBT is provided via Web Service APIs.

Each hardware device has its own driver/service provider which runs in a dedicated Docker container.  The drivers expose a Web Service API which is available (over container networking) to other Docker containers running on the same machine.

The APIs are also exposed externally to Docker (e.g. to the Chromium browser which hosts our frontend) through a "router" container, which maps each of the services onto a single HTTP endpoint.  This allows the web-based frontend to access any of the device drivers via a simple `http://localhost:8080/` endpoint.

![Diagram showing the overall architecture of the device container solution](/docs/hardware-devices/img/apis-arch-diagram.png)*Overview Diagram* 

# V2 APIs
With `V2.0.0` of the main Maramures project (that includes all the hardware
drivers etc.), most of these APIs have changed in a non-backward-compatible
way.  In the main these changes are not dramatic and do not change the
semantics of the operations concerned (although for an exception see the new
`printer-svc/v2/template` printing API), but are quality of live improvements
for JavaScript clients of the APIs.  But, when migrating, assume all APIs have
changed in some way.

## Printer Service
This service exposes the internal ticket printer.

### Method: Self Test
| Attribute | Explanation |
| -------- | ------------|
| Endpoint | (http://localhost:8080/printer-svc/v2/selfTest) |
| Method   | `GET` |
| Description | Ask the printer to print a self-test ticket |

### Method: Print Document
| Attribute | Explanation |
| -------- | ------------|
| Endpoint | (http://localhost:8080/printer-svc/v2/print) |
| Status   | **DEPRECATED**
| Method   | `POST` |
| Description | Asks the printer to print out a document. The data is provided as a JSON payload to the POST request. |
| Payload | A document describing the content to print.  See example below. |
| Notes   | Since V0.2.0, the Content-Type of the request is validated.  You must ensure your POST has content type `application/jason` |
| Errors  | For most (hah!) errors, you should receive a JSON object response describing any error encountered, e.g.: `{ "ERR": "Json deserialize error: unknown variant 'Tiitle', expected one of 'Bold', 'Title', 'Right', 'BarCode', 'HR', 'Text', 'Doc' at line 4 column 14" }`
#### Deprecation Notice
This method is now deprecated.  It will not be retired - it is conceivable that
there are corner-cases where having the caller dictate presentation as well as
content is useful - but in general it is expected that the front-end will
prefer to use the template based `/printer-svc/v2/template` endpoint described
below.

Right is reserved to change this API in breaking ways in future without bumping
the API version.
#### Example payload
```json
{
  "Doc": [
    "HR",
    { "Centre": { "Text": [ "Normal", "TimBet 2020" ]}},
    "HR",
    { "Text": [ "Bold", "Project Maramures" ]},
    { "Text": [ "Normal", "Deployed at:" ]},
    { "Right": { "Text": [ "Normal", "Tue 23 Jun 17:37:05 BST 2020" ]}},
    { "BarCode": [ "Vertical", { "string_rep": "This is some text in a 1D barcode that will be rendered vertically" }]},
    "HR",
    {"Horizontal":["Single",[{"QRCode":{"string_rep":"http://maramures.snowgoons.ro/"}},{"Text":["Normal","This is a test of a barcode with some word-wrapped text aligned next to it."]}]]}
  ]
}
```
#### Changes since `/printer-svc/v1`
Apart from the use of this method directly by clients being discouraged, the main changes
since v1 are enhancements to the formatting ability and some changes to data
structures:
* `Centre`, `Right` are no longer special types of text, but rather containers that can format their contents regardless of type (text, images, etc.)
* New `Frame` (wrap content in a box), and `Horizontal` containers (layout an array of children horizontally in a table)
* `Text` now has a style attribute that chooses between `Normal`, `Italic`, `Bold`, `Wide`, `Narrow`
* `BarCode` (for regular 1D barcodes) now has an orientation attribute that allows them to be rendered `Vertical` as well as `Horizontal`.  (`Vertical` is useful for printing "cash vouchers" that can then be read by the bill acceptor.)
* New `Image` tag that allows images to be printed (including `Percent(x)` or `ToFit` scaling), sourced either from local static assets deployed with the driver or from HTTP URLs.

### Method: Print Template
| Attribute | Explanation |
| --------- | ----------- |
| Endpoint  | (http://localhost:8080/printer-svc/v2/template/{templatename}) |
| Method    | `POST` 
| Description | Ask the printer to print the named [Handlebars] template. |
| Payload | A JSON datastructure to be passed into Handlebars as the variable data to be substituted into the named template.  Note that the payload may be JSON or JSON5 for better readability/hand-coding - if sending json5, ensure the Content-Type is `application/json5`
| Notes   | The templates are packaged with the driver itself (typically as a Kubernetes ConfigMap specific to the operator deployment) |
| Errors  | For most (hah!) errors, you should receive a JSON object response describing any error encountered, e.g.: `{ "ERR": "Json deserialize error: unknown variant 'Tiitle', expected one of 'Bold', 'Title', 'Right', 'BarCode', 'HR', 'Text', 'Doc' at line 4 column 14" }`
#### Example payload
The data format expected by the template is determined by the template itself, not the driver.
Since these templates are configurable per-operator and determined by the
operator deployment, it is not the scope of this documentation to dictate the
data format.  This example is thus simply an *example* of what such a data
structure *could* look like:
``` json
{ "bets": [ 
    {
        "success": true,
        "betId": "331db30c-a29c-4d09-9144-f7e903751469",
        "type": "MULTIPLE",
        "amount": 8,
        "currency": "EUR",
        "totalPrice": 22.43,
        "maxWinning": 179.45,
        "selections": [
            {
                "selectionId": "144368625146471168",
                "price": 6.5,
                "selection": {
                    "translationKey": "%%t('oddsMatrix_outcome_P_OR_D_STATIC_TEXT',[%%t('1004_PARTICIPANT',[])])",
                },
            },
            {
                "selectionId": "144387301064334848",
                "price": 1.1818181,
                "selection": {
                    "translationKey": "%%t('oddsMatrix_outcome_P_OR_D_STATIC_TEXT',[%%t('1004_PARTICIPANT',[])])",
                }
            },
            {
                "selectionId": "144392477224280576",
                "price": 1.877193,
                "selection": {
                    "translationKey": "%%t('oddsMatrix_outcome_P_OR_D_STATIC_TEXT',[%%t('1004_PARTICIPANT',[])])",
                },
            },
            {
                "selectionId": "144392500738596864",
                "price": 1.5555556,
                "selection": {
                    "translationKey": "%%t('oddsMatrix_outcome_P_OR_D_STATIC_TEXT',[%%t('1004_PARTICIPANT',[])])",
                }
            }
        ],
        "bonuses": [ {
            "description": "Congratulations, you have qualified for our mega-super-duper free bonus!  Enter or scan the bonus code below when you place your next bet - in-store or online!",
            "code": "FREEBET12345"
        }]
    } 
],
"account": {
  "username": "tim_walls",
  "accountId": "1234567890",
  "operatorId": "1006"
}}
```

## Card Printer Service
This service exposes the player card printer. It prints a players details and their identity barcode in plain text onto pre-printed (with the pretty stuff) RFID cards.

At the moment, the printer needs to be attached via CUPS somehow (either locally or over the network.)

The template used for positioning/styling the elements is passed to the driver container when it is started.

### Method: Self Test
| Attribute | Explanation |
| -------- | ------------|
| Endpoint | (http://localhost:8080/cardprint-svc/v1/selfTest) |
| Method   | `GET` |
| Description | Ask the printer to print a self-test card |

### Method: Print
| Attribute | Explanation |
| -------- | ------------|
| Endpoint | (http://localhost:8080/cardprint-svc/v1/print) |
| Method   | `POST` |
| Description | Ask the printer to print a card for the user
| Payload | Player information data.  The format must follow the example shown below; note that all fields are mandatory, although some (e.g. `tags`) may have an empty string.

#### Example payload
```json
{
"first_name": "Tim",
"last_name": "Walls",
"username": "tim_walls",
"user_id": "1234567890",
"operator_id": "666",
"tags": "TESTING"
}
```
#### Notes
The `tags` field is intended to contain tags or other information to be printed alongside the player's name (e.g. GM player roles) - for example this is where you would put "VIP" (but only if you want it to be printed on the card!)

The data encoded into the barcode printed on the card is determined by the template (i.e. it can be modified, but only by updating and restarting the container.) At present the template used for the barcode (i.e. what you can expect to read back from the barcode on the card) is:

```json
"id":{"uid":"${USERID}","opid":"${OPID}"}
```
I have tested, and this works OK (i.e. still fits on the card!) with user IDs up-to-and-including GUIDs.

## RFID (NFC) Reader Service
This service exposes the RFID card reader. It offers both an RPC ("get state") method, and an SSE event stream that will notify the client when an event happens.

### Method: Driver version
| Attribute | Explanation |
| -------- | ------------|
| Endpoint | (http://localhost:8080/nfc-svc/v2/driverVersion) |
| Method   | `GET` |
| Description | Ask the printer to return the version of the driver and libnfc. |

### Method: Get current state
| Attribute | Explanation |
| -------- | ------------|
| Endpoint | (http://localhost:8080/nfc-svc/v2/state) |
| Method   | `GET` |
| Description | Returns the current state of the reader as a JSON object.  The state may be either `CardPresent`, indicating a card is currently placed on the reader, or `CardNotPresent`.
#### Changes since `/nfc-svc/v1`
The state type - `CardPresent` or `CardNotPresent` is now explicitly tagged in a field `state`.  The card data (if
present) is encoded alongside in a `card` object.
#### Examples
Response when a card is not present:
```json
{"state":"CardNotPresent"}
```

Response when a card is present:
```json
{"state":"CardPresent", 
  "card":{
    "card_type":"UNDETERMINED",
    "card_uid":"c9:ce:9d:a3"
  }
}
```

### SSE: Card event stream
| Attribute | Explanation |
| -------- | ------------|
| Endpoint | (http://localhost:8080/nfc-svc/v2/events) |
| Method   | `GET` |
| Description | Delivers real-time events from the NFC reader.  There are three events - `CardPresented`, when a card is placed on the reader, `CardRemoved`, when a card is removed, and `NoChange` (which in practice you should never get, but don't crash if you do)... |
#### Changes since `/nfc-svc/v1`
The event type is now included in the data as an explicit field, `type`.  The
card data - if present - is alongside in a structure named `card`.
#### Examples
Event received when a card is presented:
```json
event: CardPresented
data: {"type":"CardPresented","card":{"card_type":"UNDETERMINED","card_uid":"c9:ce:9d:a3"}}
```

Event received when a card is removed:
```json
event: CardRemoved
data: {"type":"CardRemoved"}
```

## Barcode Scanner Service
This service exposes the barcode reader. It offers an SSE event stream that will notify the client every time a code is read.

### SSE: Barcode event stream
| Attribute | Explanation |
| -------- | ------------|
| Endpoint | (http://localhost:8080/scanner-svc/v2/events) |
| Method   | `GET` |
| Description | Delivers real-time events from the barcode scanner. There is one type of event: `BarcodeScanned`- when a barcode is scanned! The decoded barcode content is included in the payload.
#### Changes since `/scanner-svc/v1`
The event type is now included in the event data as an explicit field, `type`.
The barcode data is alongside in the fields `decoded` and `raw_data`.
#### Notes & Examples
Since v0.2.0 of the driver, we now attempt to actually validate the contents of the barcode as valid JSON before we send the event to the event stream.  We do this by decoding the JSON in the driver, and then re-encoding it before sending.  That means the client can receive an actual JSON object, rather than the stringified version we sent before.

The decoded data is sent in the `decoded` element of the response.  If we were able to successfully parse it, it will be in `decoded.Ok`, like so:
```json
event: BarcodeScanned
data: {"type":"BarcodeRead","decoded":{"Ok":1234567890},"raw_data":"1234567890"}
```

If the barcode could not be parsed as valid JSON, instead of receiving `decoded.Ok` you will get `decoded.Err`, with
an error string indicating (possibly) why it couldn't be parsed.

In either case, the original raw barcode data is in the `raw_data` field.

## Physical Buttons service
This service exposes any physical buttons on the terminal attached via the processor's
GPIO lines.  On the current cashier terminal, that means the 'Next Customer' button.

### SSE: GPIO event stream
| Attribute | Explanation |
| -------- | ------------|
| Endpoint | (http://localhost:8080/gpio-svc/v2/events) |
| Method   | `GET` |
| Description | Delivers real-time events from the physical buttons. There are two types of event: `SwitchPressed` - the corresponding button was pressed, `SwitchReleased` - the corresponding button was released.  

The name of the switch which caused the event is included in the payload. Currently this comprises:
`NextCustomer` - the button above the RFID reader
#### Changes since `/gpio-svc/v1`
The event type - `SwitchPressed` or `SwitchReleased` is now included in the data as an explicit field, `type`.  The
name of the switch is alongside in a field named `switch`.

#### Examples
Events received when the NextCustomer button is pressed and released:
```json
event: SwitchEvent
data: {"type":"SwitchPressed","switch":"NextCustomer"}

event: SwitchEvent
data: {"type":"SwitchReleased","switch":"NextCustomer"}
```



[handlebars]: https://handlebarsjs.com/